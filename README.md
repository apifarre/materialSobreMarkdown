![markdown](imatges/markdown.jpg)

:smile:

#El format Markdown#

####Afegir blockquotes####

* Amb els símbol **\>** els quals podem niuar.

		>Aquest text continua a la
        línia següent
        >>Això és un blockquote niuat

* Resultat:

>Aquest text està dins un
blockquote.
>>Això és un blockuote niuat.

####Saltar de línia####

* Podem saltar de linia fent return:

		Aquest text continua a la
        línia següent

* Resultat:

	Aquest text continua a la
    línia següent



####Taxtar text####

* Podem **tarxar** utilitzant codi HTML:

		<s>Aquest text està tatxat</s>

* Resultat:

	<s>Aquest text està tatxat</s>

####Taules####

* Podem crear taules:

	Capçalera 1 | capçalera 2
	----------- | -----------
	contingut1  | contingut 2
	contingut3  | contingut 4 


* Resultat:

	Capçalera 1 | capçalera 2
	----------- | -----------
	contingut1  | contingut 2
	contingut3  | contingut 4

####Codi####

	Això és una instrucció `printf()`

pantalla:

Això és una instrucció `printf()`

També podem tabular i queda enmarcat amb negreta. Exemple:

	Aixó és una línia tabulada.

####Subratllar####
* Markdown no té forma de fer-ho, però podem utilitzar HTML:
		<u>subratllat</u>

* Pantalla:

	<u>subratllat</u>

##Webgrafia##
* Al final d'aquest enllaç tens un munt d'eines Markdown
<http://joedicastro.com/markdown-la-mejor-opcion-para-crear-contenidos-web.html>


